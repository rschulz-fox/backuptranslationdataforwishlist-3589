import mysql from "mysql";

const connection = mysql.createConnection({
  host: "localhost",
  user: "foxbase",
  password: "root",
  database: "selectors",
});

const resultMap: Map<number, { defaultLanguage: string; config: string }> = new Map();

connection.connect((err: Error) => {
  if (err) throw err;
  console.log("Connected!");

  connection.query(
    "SELECT wp.selectorId, sl.code, s.defaultLanguage, tmpTranslations FROM `selectors`.`wishlist_page` wp INNER JOIN `selectors`.`selector` s ON wp.selectorId=s.id INNER JOIN `selectors`.`selector_language` sl ON wp.selectorId=sl.selectorId WHERE JSON_LENGTH(tmpTranslations) > 0; ",
    async (err: Error, results: any[]) => {
      if (err) throw err;

      for(const row of results) {
        const selectorId: number = row.selectorId;
        const defaultLanguage: string = row.defaultLanguage;
        const translationLanguage: string = row.code;
        const config: string = JSON.stringify(
          { [defaultLanguage]: row.tmpTranslations },
          null,
          3
        );
        if (defaultLanguage === translationLanguage) {
          resultMap.set(selectorId, { defaultLanguage, config });
        }
      }
      await saveDataToWishlistpage(resultMap); }
  );

  function saveDataToWishlistpage(resultMap: Map<number, {defaultLanguage: string; config: string}>) {
    let keys = resultMap.keys();
    for (let key of keys) {
      const selectorId: number = key;
      const tmpTranslation: string | undefined = resultMap.get(key)?.config;
      const formattedTranslation: string = JSON.stringify(tmpTranslation,null,4).replace(/^"([\s\S]*)"$/, '$1');    //to prevent strange formatting happening
      const sql = `UPDATE selectors.wishlist_page wp SET wp.tmpTranslations = '${formattedTranslation}' WHERE wp.selectorId = '${selectorId}'`;
      connection.query(sql, (err: Error, result: any) => {
        if (err) throw err;
        console.log(
          `Updated ${result.affectedRows} row(s) for selectorId ${selectorId}`
        );
      });
    }
    connection.end();
  }
});
